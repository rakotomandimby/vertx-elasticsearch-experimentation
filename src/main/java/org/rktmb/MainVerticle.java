package org.rktmb;

import com.hubrick.vertx.elasticsearch.ElasticSearchService;
import com.hubrick.vertx.elasticsearch.model.GetOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;
import javax.inject.Inject;

import    io.vertx.service.ServiceVerticleFactory ;



public class MainVerticle extends AbstractVerticle {
  
  @Override
  public void start() {

    vertx.registerVerticleFactory(new ServiceVerticleFactory());

    DeploymentOptions options = new DeploymentOptions();
    // https://vertx.io/docs/vertx-service-factory/java/
    // com.hubrick.vertx.elasticsearch.ElasticSearchServiceVerticle
    vertx.deployVerticle("service:com.hubrick.vertx.vertx-elasticsearch-service");

    // Plain
    final ElasticSearchService elasticSearchService =
        ElasticSearchService.createEventBusProxy(vertx, "eb.elasticsearch");

    final GetOptions getOptions = new GetOptions().setFetchSource(true);
    // .addField("id").addField("message");

    elasticSearchService.get(
        "twitter",
        "_doc",
        "1",
        getOptions,
        getResponse -> {
          JsonObject response = getResponse.result().toJson();
          System.out.println(response);
          
        });

    vertx
        .createHttpServer()
        .requestHandler(req -> req.response().end("Hello Vert.x!"))
        .listen(8080);
  }
}
